NAME
    AnyEvent::HTTPD - A simple lightweight event based web (application)
    server

VERSION
    Version 0.04

SYNOPSIS
        use AnyEvent::HTTPD;

        my $httpd = AnyEvent::HTTPD->new (port => 9090);

        $httpd->reg_cb (
           '/' => sub {
              my ($httpd, $req) = @_;

              $req->o ("<html><body><h1>Hello World!</h1>");
              $req->o ("<a href=\"/test\">another test page</a>");
              $req->o ("</body></html>");
              $req->respond;
           },
           '/test' => sub {
              my ($httpd, $req) = @_;

              $req->o ("<html><body><h1>Test page</h1>");
              $req->o ("<a href=\"/\">Back to the main page</a>");
              $req->o ("</body></html>");
              $req->respond;
           },
        );

        $httpd->run; # making a AnyEvent condition variable would also work

DESCRIPTION
    This module provides a simple HTTPD for serving simple web application
    interfaces. It's completly event based and independend from any event
    loop by using the AnyEvent module.

    It's HTTP implementation is a bit hacky, so before using this module
    make sure it works for you and the expected deployment. Feel free to
    improve the HTTP support and send in patches!

    The documentation is currently only the source code, but next versions
    of this module will be better documented hopefully. See also the
    "samples/" directory in the AnyEvent::HTTPD distribution for basic
    starting points.

    AnyEvent::HTTPD even comes with some basic AJAX framework/helper.

FEATURES
    *   support for GET and POST requests

    *   processing of "x-www-form-urlencoded" and "multipart/form-data"
        encoded form parameters

    *   ajax helper and javascript output functions in
        AnyEvent::HTTPD::Appgets

METHODS
    The AnyEvent::HTTPD class inherits directly from
    AnyEvent::HTTPD::HTTPServer which inherits the event callback interface
    from Object::Event.

    Event callbacks can be registered via the Object::Event API (see the
    documentation of Object::Event for details).

    For a list of available events see below in the *EVENTS* section.

    new (%args)
        This is the constructor for a AnyEvent::HTTPD object. The %args hash
        may contain one of these key/value pairs:

        port => $port
            The TCP port the HTTP server will listen on.

    stop_request
        When the server walks the request URI path upwards you can stop the
        walk by calling this method. Example:

           $httpd->reg_cb (
              '/test' => sub {
                 my ($httpd, $req) = @_;

                 # ...

                 $httpd->stop_request; # will prevent that the callback below is called
              },
              '' => sub { # this one wont be called by a request to '/test'
                 my ($httpd, $req) = @_;

                 # ...
              }
           );

    run This method is a simplification of the "AnyEvent" condition variable
        idiom. You can use it instead of writing:

           my $cvar = AnyEvent->condvar;
           $cvar->wait;

    stop
        This will stop the HTTP server and return from the "run" method if
        you started the server via that method!

EVENTS
    Every request goes to a specific URL. After a (GET or POST) request is
    received the URL's path segments are walked down and for each segment a
    event is generated. An example:

    If the URL '/test/bla.jpg' is requestes following events will be
    generated:

      '/test/bla.jpg' - the event for the last segment
      '/test'         - the event for the 'test' segment
      ''              - the root event of each request

    To actually handle any request you just have to register a callback for
    the event name with the empty string. To handle all requests in the
    '/test' directory you have to register a callback for the event with the
    name '/test'. Here is an example how to register an event for the
    example URL above:

       $httpd->reg_cb (
          '/test/bla.jpg' => sub {
             my ($httpd, $req) = @_;

             $req->respond ([200, 'ok', { 'Content-Type' => 'text/html' }, '<h1>Test</h1>' }]);
          }
       );

    See also "stop_request" about stopping the walk of the path segments.

    The first argument to such a callback is always the AnyEvent::HTTPD
    object itself. The second argument ($req) is the
    AnyEvent::HTTPD::Request object for this request. It can be used to get
    the (possible) form parameters for this request or the transmitted
    content and respond to the request.

    Also every request also emits the "request" event, with the same
    arguments and semantics, you can use this to implement your own request
    multiplexing.

CACHING
    Any response from the HTTP server will have "Cache-Control" set to
    "max-age=0" and also the "Expires" header set to the "Date" header.
    Meaning: Caching is disabled.

    If you need caching or would like to have it you can send me a mail or
    even better: a patch :)

AUTHOR
    Robin Redeker, "<elmex at ta-sa.org>"

BUGS
    Please report any bugs or feature requests to "bug-bs-httpd at
    rt.cpan.org", or through the web interface at
    <http://rt.cpan.org/NoAuth/ReportBug.html?Queue=AnyEvent-HTTPD>. I will
    be notified, and then you'll automatically be notified of progress on
    your bug as I make changes.

SUPPORT
    You can find documentation for this module with the perldoc command.

        perldoc AnyEvent::HTTPD

    You can also look for information at:

    *   RT: CPAN's request tracker

        <http://rt.cpan.org/NoAuth/Bugs.html?Dist=AnyEvent-HTTPD>

    *   AnnoCPAN: Annotated CPAN documentation

        <http://annocpan.org/dist/AnyEvent-HTTPD>

    *   CPAN Ratings

        <http://cpanratings.perl.org/d/AnyEvent-HTTPD>

    *   Search CPAN

        <http://search.cpan.org/dist/AnyEvent-HTTPD>

ACKNOWLEDGEMENTS
COPYRIGHT & LICENSE
    Copyright 2008 Robin Redeker, all rights reserved.

    This program is free software; you can redistribute it and/or modify it
    under the same terms as Perl itself.

